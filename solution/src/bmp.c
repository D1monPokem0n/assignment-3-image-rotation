#include "../include/image.h"
#include "../include/iostatus.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define PIXEL_SIZE 3
#define BYTE_SIZE 8
#define MULTIPLICITY 4

#define FILE_TYPE 19778
#define RESERVED 0
#define INFO_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define X_PELS_PER_METR 0
#define Y_PELS_PER_METR 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

/* Returns BMP header without data about image */
struct bmp_header bmp_header_new(void)
{
    return (struct bmp_header){
        .bfType = FILE_TYPE,
        .bfReserved = RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = INFO_SIZE,
        .biPlanes = PLANES,
        .biBitCount = PIXEL_SIZE * BYTE_SIZE,
        .biCompression = COMPRESSION,
        .biXPelsPerMeter = X_PELS_PER_METR,
        .biYPelsPerMeter = Y_PELS_PER_METR,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT};
}

static size_t get_image_width_padding(const uint32_t width)
{
    size_t width_mod = width * PIXEL_SIZE % MULTIPLICITY;
    if (width_mod != 0)
    {
        return MULTIPLICITY - width_mod;
    }
    return width_mod;
}

static struct bmp_header bmp_header_create(const uint64_t width, const uint64_t height)
{
    struct bmp_header header = bmp_header_new();

    header.biWidth = (uint32_t)width;
    header.biHeight = (uint32_t)height;
    header.biSizeImage = (uint32_t) ((width * PIXEL_SIZE + get_image_width_padding(width)) * height);
    header.bfileSize = header.bOffBits + header.biSizeImage;

    return header;
}

static enum read_status skip_padding_bytes(FILE *in, const size_t padding_size)
{
    if (padding_size == 0)
    {
        return READ_OK;
    }
    if (fseek(in, (long)padding_size, SEEK_CUR) != 0)
    {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    if (fseek(in, header.bOffBits, SEEK_SET) != 0)
        return READ_INVALID_BITS;

    struct pixel *pixels = pixels_create(header.biWidth, header.biHeight);

    if (pixels == NULL) return READ_INVALID_BITS;

    size_t padding_size = get_image_width_padding(header.biWidth);

    for (size_t i = 0; i < header.biHeight; i++)
    {
        for (size_t j = 0; j < header.biWidth; j++)
        {
            if (fread(pixels_get(pixels, i * header.biWidth + j), PIXEL_SIZE, 1, in) != 1)
            {
                free(pixels);
                return READ_INVALID_BITS;
            }    
        }
        if (skip_padding_bytes(in, padding_size) == READ_INVALID_BITS)
            return READ_INVALID_BITS;
    }

    image_create(img, (int64_t)header.biHeight, (int64_t)header.biWidth, pixels);

    return READ_OK;
}

static enum write_status write_padding_bytes(FILE *out, size_t padding_size)
{
    if (padding_size == 0)
        return WRITE_OK;
        
    if (fseek(out, (long)padding_size, SEEK_CUR) != 0)
        return WRITE_ERROR;

    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    struct bmp_header header = bmp_header_create(image_get_width(img), image_get_height(img));
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    size_t padding_size = get_image_width_padding(image_get_width(img));

    if (fseek(out, header.bOffBits, SEEK_SET) != 0)
        return WRITE_ERROR;

    for (size_t i = 0; i < image_get_height(img); ++i)
    {
        for (size_t j = 0; j < image_get_width(img); ++j)
        {
            if (fwrite(image_pixel_get(img, i * image_get_width(img) + j), PIXEL_SIZE, 1, out) != 1)
                return WRITE_ERROR;
        }
        if (write_padding_bytes(out, padding_size) == WRITE_ERROR)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
