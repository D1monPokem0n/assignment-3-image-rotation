#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/iostatus.h"
#include "../include/rotate.h"
#include "../include/utils.h"

#include <stdio.h>
#include <stdlib.h>

#define APP_NAME "image-transformer"

void usage(void)
{
    printf("Usage: ./" APP_NAME " <src.bmp> <dest.bmp> <angle>\n");
    printf("Available angles: 0, 90, 180, 270, -90, -180, -270\n");
}

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        printf("Invalid argumnets count");
        usage();
        getchar();
        return 0;
    }

    char *src_file_name = argv[1];
    char *dest_file_name = argv[2];

    int64_t angle = atol(argv[3]);

    if (-270 > angle || angle > 270 || angle % 90 != 0)
    {
        printf("Invalid angle: %" PRId64 "\n", angle);
        usage();
        getchar();
        return 0;
    }

    FILE *in = fopen(src_file_name, "r");
    if (in == NULL)
    {
        printf("Failed open file %s\n", src_file_name);
        getchar();
        return 0;
    }

    struct image *image = image_new();

    if (image == NULL) {
        printf("Couldn't read image\n");
        getchar();
        return 0;
    }

    switch (from_bmp(in, image))
    {
    case READ_INVALID_BITS:
        printf("Read invalid bits\n");
        image_free(image);
        getchar();
        return 0;
    case READ_INVALID_HEADER:
        printf("Read invalid header\n");
        image_free(image);
        getchar();
        return 0;
    case READ_INVALID_SIGNATURE:
        printf("Read invalid sighanture\n");
        image_free(image);
        getchar();
        return 0;
    default:
        break;
    }

    fclose(in);
  
    struct image *rotated_image = rotate_by_angle(image, angle);

    if (rotated_image == NULL) {
        printf("Couldn't rotate image\n");
        getchar();
        return 0;
    }

    FILE *out = fopen(dest_file_name, "w");
    if (out == NULL)
    {
        printf("Failed on create file %s", dest_file_name);
        getchar();
        image_free(image);
        image_free(rotated_image);
        return 0;
    }

    if (to_bmp(out, rotated_image) == WRITE_ERROR)
    {
        printf("Error on writinhg img");
        getchar();
        image_free(image);
        image_free(rotated_image);
        return 0;
    }
    fclose(out);

    image_free(image);
    image_free(rotated_image);
    printf("Success!");
    getchar();
    return 0;
}
