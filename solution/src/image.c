#include "../include/image.h"

#include <stdlib.h>

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct pixel
{
    uint8_t b, g, r;
};

struct image *image_new(void)
{
    return (struct image *)malloc(sizeof(struct image));
}

struct pixel *pixels_create(const uint64_t width, const uint64_t height)
{
    return malloc(width * height * sizeof(struct pixel));
}

void pixels_free(struct pixel *data)
{
    free(data);
}

void image_free(struct image *img)
{
    pixels_free(img->data);
    free(img);
}

void image_create(struct image *img, uint64_t height, uint64_t width, struct pixel *data)
{
    img->height = height;
    img->width = width;
    img->data = data;
}

uint64_t image_get_height(const struct image *img)
{
    return img->height;
}

uint64_t image_get_width(const struct image *img)
{
    return img->width;
}

struct pixel *image_pixel_get(const struct image *img, size_t index)
{
    return img->data + index;
}

void image_pixel_set(struct image *img, size_t index, struct pixel *pixel)
{
    img->data[index] = *pixel;
}

struct pixel *pixels_get(struct pixel *data, size_t index)
{
    return data + index;
}

void pixels_set(struct pixel *data, size_t index, struct pixel *value)
{
    data[index] = *value;
}
