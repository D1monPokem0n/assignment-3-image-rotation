#include <inttypes.h>
#include <stdlib.h>

// ASCII '0'
#define ZERO_CHAR 48

int64_t parse_int64_t(const char *str)
{
    int64_t value = 0;
    if (str[0] == '-')
    {
        for (size_t i = 1; str[i] != 0; i++)
        {
            value = 10 * value + str[i] - ZERO_CHAR;
        }
        return -value;
    }
    else
    {
        for (size_t i = 0; str[i] != 0; i++)
        {
            value = 10 * value + str[i] - ZERO_CHAR;
        }
        return value;
    }
}
