#include "../include/image.h"

#include <stdbool.h>
#include <stdlib.h>

struct image *rotate(struct image *src, bool free_reqiure)
{
    uint64_t width = image_get_width(src);
    uint64_t height = image_get_height(src);

    struct image *dest = image_new();

    if (dest == NULL) return NULL;

    struct pixel *rotated_pixels = pixels_create(width, height);
    for (size_t i = 0; i < height; i++)
    {
        for (size_t j = 0; j < width; j++)
        {
            struct pixel *src_pixel = image_pixel_get(src, i * width + j);
            pixels_set(rotated_pixels, (height) * (width - 1 - j) + i, src_pixel);
        }
    }

    image_create(dest, width, height, rotated_pixels);

    if (free_reqiure) {
        image_free(src);
    }

    return dest;
}

struct image *rotate_by_angle(struct image *const src, int64_t angle)
{
    if (angle <= 0)
        angle = 360 + angle;

    struct image *dest = rotate(src, false);

    if (dest == NULL) return NULL;

    size_t rotate_count = angle / 90;
    for (size_t i = 1; i < rotate_count; i++)
    {
        dest = rotate(dest, true);
    }

    return dest;
}
