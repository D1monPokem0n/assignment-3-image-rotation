#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"
#include <inttypes.h>
struct image *rotate_by_angle(struct image *const src, int64_t angle);
#endif
