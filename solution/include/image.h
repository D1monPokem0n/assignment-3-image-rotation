#ifndef IMAGE_H
#define IMAGE_H
#include <stddef.h>
#include <stddef.h>
#include <stdint.h>
#include <stdint.h>
// #include "image.h"
struct image;
struct pixel;
struct image *image_new(void);
void image_free(struct image *img);
void image_create(struct image *img, uint64_t height, uint64_t width, struct pixel *data);
uint64_t image_get_height(const struct image *img);
uint64_t image_get_width(const struct image *img);
struct pixel *pixels_create(const uint64_t width, const uint64_t height);
struct pixel *image_pixel_get(const struct image *img, size_t index);
void image_pixel_set(struct image *img, size_t index, struct pixel *pixel);
struct pixel *pixels_get(struct pixel *data, size_t index);
void pixels_set(struct pixel *data, size_t index, struct pixel *value);
#endif
