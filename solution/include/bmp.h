#ifndef BMP_H
#define BMP_H
#include "image.h"
#include "iostatus.h"
#include <stdio.h>
enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image const *img);
#endif
